const arr = [10, -1, 2, 5, 0, 6, 4, -5];

const bubbleSort = (arr) => {
  let len = arr.length;

  for(let i=len-1; i>=0; i--){
    for(let j=1; j<=i; j++){
      if(arr[j-1] > arr[j]){
        let temp = arr[j-1];
        arr[j-1] = arr[j];
        arr[j] = temp;
      }
    }
  }
  return arr;
}
console.log(bubbleSort(arr));